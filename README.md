My vim stuff. 

##filesystem movement
```
lazy 	mnemonic 	open file explorer
:e. 	:edit . 	at current working directory
:sp. 	:split . 	in split at current working directory
:vs. 	:vsplit . 	in vertical split at current working directory
:E 	:Explore 	at directory of current file
:Se 	:Sexplore 	in split at directory of current file
:Vex 	:Vexplore 	in vertical split at directory of current file
```


##filesystem creation 
```
command 	action
% 	create a new file
d 	create a new directory
R 	rename the file/directory under the cursor
D 	Delete the file/directory under the cursor
```

##splits


Create a vertical split using :vsp and horizontal with :sp.
+

By default, they duplicate the current buffer. This command also takes a filename:
+

:vsp ~/.vimrc

+

You can specify the new split height by prefixing with a number:
+

:10sp ~/.zshrc

+

Close the split like you would close vim:
+

:q

We can use different key mappings for easy navigation between splits to save a keystroke. So instead of ctrl-w then j, it’s just ctrl-j:
+

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"Max out the height of the current split
ctrl + w _

"Max out the width of the current split
ctrl + w |

"Normalize all split sizes, which is very handy when resizing terminal
ctrl + w =

## visual selectio
```
Press v to begin character-based visual selection, or V to select whole lines, or Ctrl-v or Ctrl-q to select a block.
Move the cursor to the end of the text to be cut/copied. While selecting text, you can perform searches and other advanced movement.
Press d (delete) to cut, or y (yank) to copy.
Move the cursor to the desired paste location.
Press p to paste after the cursor, or P to paste before.
```

## End of Line

To go to end of line and past the last character: 

```
:set virtualedit=onemore
```
